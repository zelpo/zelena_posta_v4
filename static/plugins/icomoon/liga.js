/* A polyfill for browsers that don't support ligatures. */
/* The script tag referring to this file must be placed before the ending body tag. */

/* To provide support for elements dynamically added, this script adds
   method 'icomoonLiga' to the window object. You can pass element references to this method.
*/
(function () {
	'use strict';
	function supportsProperty(p) {
		var prefixes = ['Webkit', 'Moz', 'O', 'ms'],
			i,
			div = document.createElement('div'),
			ret = p in div.style;
		if (!ret) {
			p = p.charAt(0).toUpperCase() + p.substr(1);
			for (i = 0; i < prefixes.length; i += 1) {
				ret = prefixes[i] + p in div.style;
				if (ret) {
					break;
				}
			}
		}
		return ret;
	}
	var icons;
	if (!supportsProperty('fontFeatureSettings')) {
		icons = {
			'barchart': '&#xf080;',
			'piechart': '&#xf200;',
			'apple': '&#xf179;',
			'android': '&#xf17b;',
			'twitter': '&#xf081;',
			'facebook': '&#xf082;',
			'linkedin': '&#xf08c;',
			'google': '&#xf0d4;',
			'laptop': '&#xf109;',
			'phone': '&#xf10b;',
			'youtube': '&#xf167;',
			'print': '&#xf02f;',
			'upload': '&#xf093;',
			'spinner': '&#xf110;',
			'link': '&#xf0c1;',
			'closesmall': '&#xf00d;',
			'trash': '&#xf014;',
			'clock': '&#xf017;',
			'info': '&#xf05a;',
			'lightbulb': '&#xf0eb;',
			'notefill': '&#xf15c;',
			'flagfill': '&#xf024;',
			'bellfill': '&#xf0f3;',
			'note': '&#xf0f6;',
			'flag': '&#xf11d;',
			'euro': '&#xf153;',
			'user': '&#xf007;',
			'settings': '&#xf013;',
			'navicon': '&#xf0c9;',
			'chevronleft': '&#xf053c;',
			'chevronright': '&#xf054;',
			'chevronup': '&#xf077;',
			'chevrondown': '&#xf078;',
			'search': '&#xf002;',
			'message': '&#xf003;',
			'check': '&#xf00c;',
			'download': '&#xf019;',
			'edit': '&#xf040;',
			'calendar': '&#xf073;',
			'bell': '&#xf0a2;',
			'caretdown': '&#xf0d7;',
			'caretup': '&#xf0d8;',
			'caretleft': '&#xf0d9;',
			'caretright': '&#xf0da;',
			'doubleleft': '&#xf100;',
			'doubleright': '&#xf101;',
			'doubleup': '&#xf102;',
			'doubledown': '&#xf103;',
			'copy': '&#xf0c5;',
			'city': '&#xe605;',
			'tipos': '&#xe604;',
			'lasso': '&#xe603;',
			'internalfile': '&#xe601;',
			'externalfile': '&#xe600;',
			'close': '&#xe606;',
			'arrowup': '&#xe608;',
			'arrowright': '&#xe609;',
			'arrowdown': '&#xe60a;',
			'arrowleft': '&#xe60b;',
			'eye': '&#xe602;',
			'0': 0
		};
		delete icons['0'];
		window.icomoonLiga = function (els) {
			var classes,
				el,
				i,
				innerHTML,
				key;
			els = els || document.getElementsByTagName('*');
			if (!els.length) {
				els = [els];
			}
			for (i = 0; ; i += 1) {
				el = els[i];
				if (!el) {
					break;
				}
				classes = el.className;
				if (/icon-/.test(classes)) {
					innerHTML = el.innerHTML;
					if (innerHTML && innerHTML.length > 1) {
						for (key in icons) {
							if (icons.hasOwnProperty(key)) {
								innerHTML = innerHTML.replace(new RegExp(key, 'g'), icons[key]);
							}
						}
						el.innerHTML = innerHTML;
					}
				}
			}
		};
		window.icomoonLiga();
	}
}());