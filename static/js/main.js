var bxSlider;

$(document).ready(function(){

  // apple touch devices click fix
  /iP/i.test(navigator.userAgent) && $('*').css('cursor', 'pointer');

  ie();

  Toggle.init();
  $('.js-select').selectpicker();
  InputSelect.init();
  $('.js-switch').bootstrapSwitch();
  DatePicker.init();
  $('.js-timepicker').clockpicker();
  $('.js-autosize').autosize();
  Tables.init();

  DashboardReminders.init();
  DashboardReminders.addReminders([
    [2014,11,25],
    [2014,11,1],
    [2014,12,24],
  ]);
  DashboardReminders.removeReminder(2014,11,1);
  
  $('.js-nolinebreak').noLineBreak();

  bxSlider = BxSlider;
  bxSlider.init();

  DashboardPayments.init();

  Lightbox.init();
  
  Tooltips.init();
  LinkCopy.init();
  
  Dropzone.init();

  if (jQuery().fileinput) {
    $('.js-fileinput').fileinput();
  }
  $('.js-marking-tool').length !== 0 ? MarkingTool.init() : '';
  
  Tabs.init();
  RadioWithInput.init();

  var events = [
    {
      id: 1,
      title: 'Zaplatiť Orange',
      start: '2014-12-03T18:00:00',
      allDay: false
    },
    {
      id: 2,
      title: 'Poslať faktúru',
      start: '2014-12-08T11:30:00',
      allDay: false
    },
    {
      id: 2,
      title: 'Zaplatiť O2',
      start: '2014-12-08T12:00:00',
      allDay: false
    },
    {
      id: 2,
      title: 'Zaplatiť mobil',
      start: '2014-12-08T12:15:00',
      allDay: false
    },
    {
      id: 2,
      title: 'Zaplatiť elektrinu',
      start: '2014-12-08T14:00:00',
      allDay: false
    },
    {
      id: 1,
      title: 'Zaplatiť elektrinu',
      start: '2015-01-01T14:00:00',
      allDay: false
    },
    {
      id: 1,
      title: 'Dumb Event',
      start: '2014-01-05T14:00:00',
      allDay: false
    },
  ]
  Calendar.init(events);
  
  Sticky.init();
  ScrollSpy.init();
  SideMenu.init();

  RadioToggle.init();
  
  $('.disabled').on('click', function(e) {
    // this shoudn't unbind custom events
    // custom events shoud be handled elsewhere
    e.preventDefault();
    e.stopPropagation();
    return false;
  });

  // Charts init
  $('#chart').length !== 0 ? Charts.init(chartData) : '';

});

function ie(){  
  var iev=0;
  var ieold = (/MSIE (\d+\.\d+);/.test(navigator.userAgent));
  var trident = !!navigator.userAgent.match(/Trident\/7.0/);
  var rv=navigator.userAgent.indexOf("rv:11.0");

  if (ieold) iev=new Number(RegExp.$1);
  if (navigator.appVersion.indexOf("MSIE 10") != -1) iev=10;
  if (trident&&rv!=-1) iev=11;

  if(iev != 0) {
    $('html').addClass('ie');
    console.log('ie');
  } else {
    console.log('noie');
  }
}

// ---------------------------------------------------
// Toggle
// global object fot handling Toggle

  var Toggle = {
    init: function(){
      this.$elements = $('.js-toggle');
      this.$dropdown = null;
      this.$toggle = null;
      this.binds();
    },
    binds: function(){
      var $this = this;
      $this.$elements.off('click');
      $this.$elements.on('click', function(e){
        $this.doToggle($(this));
        $(document).trigger('toggle-click');
        e.stopPropagation();
      })
      // if it's dropdown hide when click outside
      $(document).on('click bootstrap-click', function(e) {
        if ($this.$dropdown) {
          $('dropdown-toggle.open').trigger('click');
          $this.hide();
        }
      });
      // if it's hide when esc
      $(document).on('keydown', function(e) {
        if ($this.$dropdown && e.keyCode === 27) {
          $this.hide();
        }
      });
    },
    doToggle: function($toggle){
      var $target;

      if($toggle.data('target') == 'parent') {
        $target = $($toggle.parent());
      } else {
        $target = $($toggle.data('target'));
      }

      if(this.$dropdown && !this.$dropdown.is($target)) {
        this.hide();
      }

      $target.is('.active') ? this.hide($target, $toggle) : this.show($target, $toggle);
    },
    show: function(target, $toggle){
      if($toggle.data('toggle') == 'dropdown') {
        this.$dropdown = target;
        this.$toggle = $toggle;
      }

      (this.$dropdown || target).addClass('active');
      (this.$toggle || $toggle).addClass('active');
      this.toggleIcon(this.$toggle || $toggle);
      this.toggleText(this.$toggle || $toggle);
    },
    hide: function(target, $toggle){
      (this.$dropdown || target).removeClass('active');
      (this.$toggle || $toggle).removeClass('active');
      this.toggleIcon(this.$toggle || $toggle);
      this.toggleText(this.$toggle || $toggle);

      this.$dropdown = null;
      this.$toggle = null;
    },
    toggleIcon: function($toggle){
      if($toggle.data('toggle-icon')) {

        var oldIcon = ($toggle.find('.icon').text());
        var newIcon = ($toggle.attr('data-toggle-icon'));

        $toggle.find('.icon').text(newIcon);
        $toggle.attr('data-toggle-icon', oldIcon);
      }
    },
    toggleText: function($toggle){
      if($toggle.data('toggle-text')) {

        var oldText = $toggle.text();
        var newText = $toggle.attr('data-toggle-text');

        $toggle.text(newText);
        $toggle.attr('data-toggle-text', oldText);
      }
    }
  } // /togglers

// ---------------------------------------------------
// InputSelect
// script for  custom combiation of input and select

  var InputSelect = {
    init: function() {
      this.elements = $('.js-input-select');

      this.binds();
    },
    binds: function(){
      var that = this;

      that.elements.each(function(){
        $(this).find('select').on('change', function(){
          that.setInput($(this).parent().parent());
        })
      });
    },
    setInput: function($element){
      var text = $element.find('select option:selected').text();
      var $input = $element.find('input:text');

      $input.val(text);
    }
  }

// ---------------------------------------------------
// DatePicker
// script for custom initialization of datePicker plugin

  var DatePicker = {
    init: function() {
      this.datepickerEls = $('.js-datepicker');
      this.datepickerBtns = $('.js-datepicker-btn');

      this.datepickerEls.each(function(){
        $(this).datepicker({
          'weekStart': 1,
          'language': 'sk',
          startView: $(this).attr('data-view') ? parseInt($(this).attr('data-view'),10) : 0,
          minViewMode: $(this).attr('data-view') ? parseInt($(this).attr('data-view'),10) : 0
        })
      })

      this.datepickerInline = $('.js-datepicker-inline');
      this.datepickerInline.datepicker({
        todayHighlight: true,
        disableDayRow: true,
        language: 'sk'
      });

      this.binds();
    },
    binds: function() {
      this.datepickerBtns.on('click', function(){
        var datepickerEl = $(this).closest('.form-control').find('.js-datepicker');
        if(datepickerEl.hasClass('open')) {
          datepickerEl.datepicker('hide');
        } else {
          datepickerEl.datepicker('show');
        }
      })
    }
  }
  
  // ---------------------------------------------------
  // DashboardReminders
  // inline calendar on dashboard
  
  var DashboardReminders = {
    config : {
      sk: {
        yesterday: 'Včera',
        today: 'Dnes',
        tommorow: 'Zajtra'
      }
    },
    init: function() {
      
      this.displayedDate = $('.js-reminders .date');
      this.displayedDateNext = $('.js-reminders .next');
      this.displayedDatePrev = $('.js-reminders .prev'); 
      
      this.datepickerInline = $('.js-datepicker-inline');
      this.datepickerInline.datepicker({
        todayHighlight: true,
        disableDayRow: true,
        language: 'sk'
      });

      this.binds();
    },
    binds: function() {
      
      var that = this;
      
      var today = new Date();
      var day = today.getDate();
      var month = today.getMonth();
      var year = today.getFullYear();
      
      this.currentDate = today;
      
      this.datepickerInline.datepicker('setDate', new Date(year,month,day));
      this.setDayToDisplay(this.datepickerInline.datepicker('getDate'));
      
      this.datepickerInline.datepicker().on('changeDate', function(e){
        if (typeof e.date !== 'undefined') {
          that.currentDate = new Date(e.date);
          that.setDayToDisplay(that.currentDate);
        }
      });
      
      this.displayedDateNext.on('click', function() {
        var nextDay = new Date(that.currentDate.getFullYear(), that.currentDate.getMonth(), that.currentDate.getDate() + 1);
        that.currentDate = nextDay;
        that.setDayToDisplay(nextDay);
        that.datepickerInline.datepicker('setDate', that.currentDate);
      });
      
      this.displayedDatePrev.on('click', function() {
        var prevDay = new Date(that.currentDate.getFullYear(), that.currentDate.getMonth(), that.currentDate.getDate() - 1);
        that.currentDate = prevDay;
        that.setDayToDisplay(that.currentDate);
        that.datepickerInline.datepicker('setDate', that.currentDate);
      });
      
    },
    setDayToDisplay: function(date) {
      var newDate = new Date(date);
      var today = new Date();

      if ((today.getDate() - 1) === newDate.getDate()  &&  //yesterday
        today.getMonth() === newDate.getMonth() &&
        today.getYear() === newDate.getYear()
      ) {
        this.displayedDate.html(this.config.sk.yesterday);
      } else if ((today.getDate()) === newDate.getDate()  && //today
        today.getMonth() === newDate.getMonth() &&
        today.getYear() === newDate.getYear()
      ) {
        this.displayedDate.html(this.config.sk.today);
      } else if ((today.getDate() + 1) === newDate.getDate()  && //tommorow
        today.getMonth() === newDate.getMonth() &&
        today.getYear() === newDate.getYear()
      ) {
        this.displayedDate.html(this.config.sk.tommorow);
      } else {
        this.displayedDate.html(
          newDate.getDate() + 
          '.' + 
          (newDate.getMonth() + 1) +
          '.' + 
          newDate.getFullYear() ); //other date
      }
    },
    addReminders: function(reminders) {
      for ( var i = 0, l = reminders.length; i < l; i++ ) {
        this.datepickerInline.datepicker('addReminder', reminders[i]);
      }
      // Don't ask
      this.datepickerInline.datepicker('setDate', new Date(this.currentDate.getFullYear(),this.currentDate.getMonth(),this.currentDate.getDate()));
    },
    removeReminder: function(year,month,day) {
      this.datepickerInline.datepicker('removeReminder', year,month,day);
      // Don't ask
      this.datepickerInline.datepicker('setDate', new Date(this.currentDate.getFullYear(),this.currentDate.getMonth(),this.currentDate.getDate()));
    }
  };
  
  // ---------------------------------------------------
  // Dashboard Payments
  // custom month switcher
  
  var DashboardPayments = {
    config: {
      sk:  {
        full: ["Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"],
        short: ["Jan", "Feb", "Mar", "Apr", "Máj", "Jún", "Júl", "Aug", "Sep", "Okt", "Nov", "Dec"]
      }
    },
    init: function() {
      this.monthsSwitcher = $('.js-months-switcher');
      this.datepickerPayments = $('.js-datepicker-payments');
      this.date = $('.js-months-switcher .date');
      this.dateShort = $('.js-months-switcher .date.short');
      this.nextMonth = $('.js-months-switcher .next');
      this.prevMonth = $('.js-months-switcher .prev'); 
      
      this.datepickerPayments.datepicker({
        minViewMode: 1,
        startView: 1,
        language: 'sk'
      });
      
      this.currentMonth = new Date();
      
      this.setMonthToDisplay(this.currentMonth);
      
      this.binds();
    },
    binds: function() {
      var that = this;
      
      this.date.on('click', function(e) {
        if (that.monthsSwitcher.hasClass('open')) {
          that.datepickerPayments.datepicker('hide');
          that.monthsSwitcher.removeClass('open');
        } else {
          that.datepickerPayments.datepicker('show');
          that.monthsSwitcher.addClass('open');
        }
        
      });
      
      this.datepickerPayments.on('hide', function(){
        that.monthsSwitcher.removeClass('open');
      });
      
      this.datepickerPayments.datepicker().on('changeDate', function(e) {
        that.datepickerPayments.datepicker('hide');
        that.monthsSwitcher.removeClass('open');
        that.currentMonth = e.date;
        that.setMonthToDisplay(e.date);
      });
      
      this.nextMonth.on('click', function() {
        that.currentMonth = new Date(that.currentMonth.getFullYear(), that.currentMonth.getMonth() + 1, that.currentMonth.getDate() + 1);
        that.datepickerPayments.datepicker('setDate', that.currentMonth);
        that.setMonthToDisplay(that.currentMonth);
      });
      
      this.prevMonth.on('click', function() {
        that.currentMonth = new Date(that.currentMonth.getFullYear(), that.currentMonth.getMonth() - 1, that.currentMonth.getDate());
        that.datepickerPayments.datepicker('setDate', that.currentMonth);
        that.setMonthToDisplay(that.currentMonth);
      });
    },
    setMonthToDisplay: function(date) {
        this.date.html(this.config.sk.full[date.getMonth()] + ' ' + date.getFullYear());
        this.dateShort.html(this.config.sk.short[date.getMonth()] + ' ' + date.getFullYear());
    }
  };

// ---------------------------------------------------
// NoLineBreak

  $.fn.noLineBreak = function(){
    $elements = $('.js-nolinebreak');

    $elements.on('keypress', function(e) {
      if(e.which == '13') return false;
    })
  }
  
// ---------------------------------------------------
// Tables
// scripts for tables

  var Tables = {
    init: function(){
      this.tables = $('.js-table');

      this.checkSelected();
      this.binds();
    },
    binds: function() {
      var that =  this;

      that.tables.each(function(){
        var $table = $(this);
        var $checkboxes = $table.find('input:checkbox');

        if($checkboxes.length != 0) {

          $checkboxes.on('change', function() {
            if($(this).data('check') === 'all') {
              that.setSelectedAll($(this));
            } else {
              that.setSelected($(this));
            }
          });
        }
      });
    },
    checkSelected: function(){
      var that =  this;

      that.tables.each(function(){
        var $table = $(this);
        var $checkboxes = $table.find('input:checkbox');

        $checkboxes.each(function() {
          that.setSelected($(this));
        });
      });
    },
    setSelected: function($checkbox){
      $checkbox.closest('tr').toggleClass('selected',$checkbox.is(':checked'));
    },
    setSelectedAll: function($checkbox) {
      var $checkboxes = $checkbox.closest('.table').find('input:checkbox');
      if($checkbox.is(':checked')) {
        this.selectAll($checkboxes);
      } else {
        this.deselectAll($checkboxes);
      }
    },
    selectAll: function($checkboxes){
      var that = this;

      $checkboxes.each(function() {
        $(this).attr('checked','checked');
        that.setSelected($(this));
      });
    },
    deselectAll: function($checkboxes){
      var that = this;

      $checkboxes.each(function() {
        $(this).removeAttr('checked');
        that.setSelected($(this));
      });
    }
  }
  
// ---------------------------------------------------
// BxSlider

  var BxSlider = {
    init: function() {
      this.slider = $('.bxslider');
      if (!jQuery().bxSlider || !this.slider.length) return;
      this.slider.bxSlider({
        infiniteLoop: false,
        pagerType: 'short',
        pagerShortSeparator: ' z ',
        controls: true,
        nextText: '<i class="icon icon-m">chevronright</i>',
        prevText: '<i class="icon icon-m">chevronleft</i>',
        preventDefaultSwipeX: false ,
        adaptiveHeight: true,
        touchEnabled: false
      });
    },
    reloadSlider: function(){
      this.slider.reloadSlider();
    }
  };

// ---------------------------------------------------
// LightBox

  var Lightbox = {
    init: function() {
      this.lightBox = $('.lb [rel="lightbox"]');
      if (!jQuery().lightbox || !this.lightBox.length) return;
      this.lightBox.lightbox({ 
        blur: false
      });
    }
  };
  
// ---------------------------------------------------
// Tooltips

  var Tooltips = {
    init: function() {
      this.tooltipIcon = $('.js-tooltip-icon');
      this.tooltip = $('.js-tooltip');
      this.tooltipIcon.tooltipster({
        maxWidth: '250',
        icon: '?',
        iconDesktop: true,
        iconTouch: true,
        interactive: true
      });

      this.tooltip.tooltipster({
        maxWidth: '250',
        interactive: true
      });
    }
  };

// ---------------------------------------------------
// Link Copy Modal
  
  var LinkCopy = {
    init: function() {
      this.modalLink = $('[data-modal-id="popup-link"]');
      this.inputEl = this.modalLink.find('.input');
      this.binds();
    },
    binds: function() {
      var that = this;
      $(document).on('opened', this.modalLink, function () {
        that.inputEl.focus().select();
      });
    }
  };

// ---------------------------------------------------
// Dropzone


  var Dropzone = {
    init: function() {
      this.dropzoneEl = $('.dropzone');
      
      if (!jQuery().dropzone || !this.dropzoneEl.length) return;
      
      this.dropzoneEl.dropzone({ 
        url: "/file/post",
        uploadMultiple: false,
        acceptedFiles: 'application/pdf',
        previewsContainer: false,
        createImageThumbnails: false,
        dictDefaultMessage: '<div class="dropzone_icon"><i class="icon icon-l">upload</i></div><div class="dropzone_text">Sem vložte PDF súbor, ktorý chcete poslať.</div>',
        init: function() {
          var that = this;
          this.on("addedfile", function(file) { 
            //this.element.style.display = 'none';
          });
          this.on('error', function(file) {
            this.disable();
            this.element.innerHTML = '<div class="dropzone_message text-color-red"><i class="icon icon-m">close</i> Objavila sa chyba</div>';
          });
          this.on('sending', function(file) {
            this.element.innerHTML = '<div class="dropzone_message"><i class="icon icon-m icon-spin">spinner</i></div>';
          });
          this.on('success', function(file) {
            this.element.innerHTML = '<div class="dropzone_message text-color-green"><i class="icon icon-m">check</i> Súbor odoslaný</div>';
          });
        }
      });
      
    }
  }

// ---------------------------------------------------
// TABS
  
  var Tabs = {
    init: function() {
      this.tabs = $('.js-tabs');
      this.binds();
    },
    binds: function() {
      this.tabs.on('click', '[data-tab]', function() {
        
        var tablist = $(this).closest('.js-tabs');
        var tabpanel = $(tablist.data('tabpanel'));
        
        // set active tab in tablist
        tablist.find('[data-tab]').removeClass('active');
        $(this).addClass('active');
        
        // set active tab in tabpanel
        tabpanel.find('[role="tabpanel"]').removeClass('active');
        tabpanel.find($(this).data('tab')).addClass('active');
        
      });
    }
  };
  
// ---------------------------------------------------
// RADIO OR CHECKBOX WITH INPUT INSIDE
  
  var RadioWithInput = {
    init: function () {
      this.group = $('.radio-group');
      
      this.binds();
    },
    binds: function() {
      var that = this;
      this.group.find('input[type="radio"]').on('change', function() {
        //each radio with input in radio radio-group
        that.group.find('.radio.with-input').each(function() {
          
          if ($(this).find('[type="radio"]').is(':checked')) {
            // radio is checked
            $(this).find('[type="text"]').prop('disabled', false);
          } else {
            // radio is not checked
            $(this).find('[type="text"]').prop('disabled', true);
          }
        });
      });
    }
  };

// ---------------------------------------------------
// STICKY.JS

var Sticky = {
  init: function() {
    this.stickyEl = $('.js-sticky');
    if (!jQuery().sticky || !this.stickyEl.length) return;
    
    this.stickyEl.sticky({
      topSpacing: 0, 
      getWidthFrom: $('.js-sticky').parent(),
      responsiveWidth: true
    });
  }
};
  
// ---------------------------------------------------
// SCROLLSPY

  var ScrollSpy = {
    init: function() {
      this.scrollspyEl = $('.js-scrollspy');
      if (!jQuery().scrollspy || !this.scrollspyEl.length) return;
      this.binds();
      
      $('body').scrollspy({ 
        target: '.js-scrollspy',
        offset: 200
      });
    },
    binds: function() {}
  };
  
// ---------------------------------------------------
// SIDEMENU

  var SideMenu = {
    init: function() {
      this.sideMenuEl = $('.js-sidemenu');
      this.binds();
    },
    binds: function() {
      this.sideMenuEl.find('a').on('click', function(e) {
        // prevent default a action
        e.preventDefault();
        
        //data-target
        var target = $(e.target).data('target');
        
        //scroll to data-target
        $('html, body').animate({
          scrollTop: $(target).offset().top - 40
        }, 300);
        
      });
    }
  };
  
// ---------------------------------------------------
// Marking Tool
// scripts for marking tool

  var MarkingTool = {
    init: function(){
      this.markingTool = $('.js-marking-tool');
      this.markerSelection = this.markingTool.find('.js-marker-selection');
      this.img = this.markingTool.find('.js-marking-tool-img');
      this.closeBtn = this.markingTool.find('.js-close-marking-tool');
      this.confirmBtn = this.markingTool.find('.js-confirm-marking-tool');
      this.openBtns = $('.js-open-marking-tool');
      this.toDisable = $('.js-mark-input');
      this.inputValue,
      this.inputPosition,
      this.inputSize,
      this.currentEl,
      this.imgW,
      this.imgH,
      this.markerPosX,
      this.markerPosY,
      this.markerW,
      this.markerH;

      this.markerSelection.draggable({
        'containment': this.markingTool
      });
      this.markerSelection.resizable({
        'containment': this.markingTool,
        'handles': 'ne, nw, se, sw, n, e, s, w'
      });

      this.binds();
    },
    binds: function(){
      var that =  this;

      this.openBtns.on('click', function(){
        that.openMarkingTool($(this));
      })

      this.closeBtn.on('click', function(){
        that.closeMarkingTool();
      })

      this.confirmBtn.on('click', function(){
        that.confirmMarkingTool();
      })
    },
    openMarkingTool: function($current){

      $('.bx-wrapper').hide(); // hide slider
      this.markingTool.addClass('active'); // show marking tool
      this.disableAll();

      this.currentEl = $current;
      this.img.attr('src', this.currentEl.data('mark-img')); // show image

      var id = this.currentEl.data('mark-id');
      this.inputPosition = $('.js-mark-position[data-mark-id="' + id + '"]');
      this.inputSize = $('.js-mark-size[data-mark-id="' + id + '"]');
      this.inputValue = $('.js-mark-input[data-mark-id="' + id + '"]')

      if(this.inputPosition.val() != '' && this.inputSize != '') {
        this.getData();
      }
    },
    closeMarkingTool: function(){

      this.markingTool.removeClass('active');
      $('.bx-wrapper').show();
      bxSlider.reloadSlider();
      this.enableAll();
      this.markerSelection.removeAttr('style')

      this.markerX = 0;
      this.markerY = 0;
      this.markerW = 0;
      this.markerH = 0;
    },
    confirmMarkingTool: function(){

      this.setData();
      this.closeMarkingTool();

    },
    setData: function(){
      this.imgW = this.img.width(),
      this.imgH = this.img.height();

      this.markerX = pxToPerc(this.imgW, parseInt(this.markerSelection.css('left'),10));
      this.markerY = pxToPerc(this.imgH, parseInt(this.markerSelection.css('top'),10));
      this.markerW = pxToPerc(this.imgW, parseInt(this.markerSelection.innerWidth(),10));
      this.markerH = pxToPerc(this.imgH, parseInt(this.markerSelection.innerHeight(),10));

      // console.log(
      //   'x: ' + this.markerX + 
      //   ', y: ' + this.markerY +
      //   ', w: ' + this.markerW +
      //   ', h: ' + this.markerH
      // );

      var id = this.currentEl.data('mark-id');
      this.inputPosition.val(this.markerX + ';' + this.markerY);
      this.inputSize.val(this.markerW + ';' + this.markerH);
      this.inputValue.val('Čaká na rozpoznanie').attr('disabled','disabled').addClass('js-mark-input-disabled');

      if(this.currentEl.attr('data-mark-text')) {
        this.currentEl.text(this.currentEl.data('mark-text'));
      }

      function pxToPerc(full, part) {
        var onePerc = full / 100;
        return (part / onePerc);
      }
    },
    getData: function(){
      this.imgW = this.img.width(),
      this.imgH = this.img.height();

      this.markerX = percToPx(this.imgW, parseFloat(this.inputPosition.val().split(';')[0]));
      this.markerY = percToPx(this.imgH, parseFloat(this.inputPosition.val().split(';')[1]));
      this.markerW = percToPx(this.imgW, parseFloat(this.inputSize.val().split(';')[0]));
      this.markerH = percToPx(this.imgH, parseFloat(this.inputSize.val().split(';')[1]));

      // console.log(
      //   'x: ' + this.markerX + 
      //   ', y: ' + this.markerY +
      //   ', w: ' + this.markerW +
      //   ', h: ' + this.markerH
      // );

      this.markerSelection.css({
        'top': this.markerY + 'px',
        'left': this.markerX + 'px',
        'width': this.markerW + 'px',
        'height': this.markerH + 'px'
      });

      function percToPx(full, part) {
        var onePerc = full / 100;
        return (part * onePerc);
      }
    },
    disableAll: function(){

      this.openBtns.addClass('disabled');
      this.toDisable.attr('disabled','disabled');

    },
    enableAll: function(){

      this.openBtns.removeClass('disabled');
      this.toDisable.each(function(){
        $(this).hasClass('js-mark-input-disabled') ? '' : $(this).removeAttr('disabled');
      });

    }
  }

// ---------------------------------------------------
// Calendar
// settings for full calendar plugin

  var Calendar = {
    init: function(events){
      this.calendar = $('.js-calendar');
      this.events = events;

      this.view = this.calendar.data('calendar-view');

      // layouts breakpoints
      this.layoutXS = 480;
      this.layoutS = 768;
      this.layoutM = 1030;

      this.currentLayout;
      this.aspectRatio;
      this.titleFormat;
      this.eventLimit;

      // only if there is calendar
      if(this.calendar.length !== 0) {

        // counting layoyt, aspect ratio and title format
        this.currentLayout = this.getLayout(getViewport().width);
        this.aspectRatio = this.getAspectRatio();
        this.titleFormat = this.getTitleFormat();
        this.eventLimit = this.view === 'month' ? this.getEventLimit() : 1;

        // init of fullCalendar
        this.calendar.fullCalendar({
          defaultView: this.view,
          lang: 'sk',
          header: {
            left: 'prev,title,next',
            right: 'today'
          },
          timeFormat: 'HH:mm',
          titleFormat: {
            year: 'YYYY',
            month: this.titleFormat
          },
          weekNumbers: true,
          weekNumberTitle: 'Týždeň',
          fixedWeekCount: this.view === 'month' ? false : 6,
          columnFormat: {
            year: 'dddd',
            month: 'dddd'
          },
          buttonIcons: {
            prev: 'icon-chevronleft',
            next: 'icon-chevronright'
          },
          eventSources: [
            {
              events: this.events,
              color: '#62c05d',
              textColor: 'white'
            }
          ],
          eventLimit: this.eventLimit,
          aspectRatio: this.aspectRatio,
          //handleWindowResize: this.view === 'month' ? true : false,
          eventClick: function(calEvent, jsEvent, view) {
            var inst = $.modal.lookup[$('[data-modal-id=modal-show-reminder-' + calEvent.id + ']').data('modal')];
            inst ? inst.open() : console.log('Calendar error: cannot open event.');
          }
        });

        // binds
        this.binds();
      }

    },
    binds:function(){
      var that = this,
          didResize = false;

      if(that.view === 'month') { // resize handling is needed only for month view
        $(window).on('resize', function(){
          didResize = true;
        });

        // better performance if it doesnt trigger everytime that is resized
        setInterval(function(){
          if(didResize) {
            that.hasResized(); // all stuff that need to happen after resize
            didResize = false;
          }
        }, 250);
      }
      
    },
    hasResized: function(){

      this.currentLayout = this.getLayout(getViewport().width); // set new layout

      // to avoid render every resize, render only if ratio has changed
      if (this.aspectRatio !== this.getAspectRatio()) { 
        this.aspectRatio = this.getAspectRatio();
        this.calendar.fullCalendar('option', 'aspectRatio', this.aspectRatio);
      }

      // to avoid render every resize, render only if titleFormat has changed
      if (this.titleFormat !== this.getTitleFormat()) { 
        this.titleFormat = this.getTitleFormat();
        this.calendar.fullCalendar('option', 'titleFormat', this.titleFormat);
      }

      // to avoid render every resize, render only if eventLimit has changed
      if (this.eventLimit !== this.getEventLimit()) { 
        this.eventLimit = this.getEventLimit();
        this.calendar.fullCalendar('option', 'eventLimit', this.eventLimit);
      }

    },
    getLayout: function(viewport) {
      if(viewport < this.layoutXS) return 'XS'
      else if(viewport >= this.layoutXS && viewport < this.layoutS) return 'S'
      else if(viewport >= this.layoutS && viewport < this.layoutM) return 'M'
      else if(viewport >= this.layoutM) return 'L'
      else {
        console.log('Calendar error: cannot get layout.');
        return null;
      }
    },
    getAspectRatio: function() {

      if(this.currentLayout === 'XS') return 0.85
      else if(this.currentLayout === 'S') return 1.1
      else if(this.currentLayout === 'M' || this.currentLayout === 'L') return 1.35
      else {
        console.log('Calendar error: cannot get ratio.');
        return null;
      }
    },
    getTitleFormat: function() {

      if(this.currentLayout === 'XS') return 'MMM YYYY';
      else return 'MMMM YYYY';

    },  
    getEventLimit: function() {

      if(this.currentLayout === 'XS') return 1;
      else return true;

    }  
  }

// ---------------------------------------------------
// getViewport
// script for getting real viewport of document

  var getViewport = function() {
    var viewport = {
      'width': 0,
      'height': 0
    }

    // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
    if (typeof window.innerWidth != 'undefined') {
      viewport.width = window.innerWidth;
      viewport.height = window.innerHeight

    }
   
    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
    else if (typeof document.documentElement != 'undefined'
      && typeof document.documentElement.clientWidth !=
      'undefined' && document.documentElement.clientWidth != 0)
    {
      viewport.width = document.documentElement.clientWidth
      viewport.height = document.documentElement.clientHeight
    }
   
    // older versions of IE
    else {
      viewport.width = document.getElementsByTagName('body')[0].clientWidth
      viewport.height = document.getElementsByTagName('body')[0].clientHeight
    }

    return viewport;
  } // getViewport

// ---------------------------------------------------
// RADIO TOGGLE

  var RadioToggle = {
    init: function() {
      this.radioToggleEl = $('.js-radio-toggle');
      this.binds();
    },
    binds: function() {
      this.radioToggleEl.find('[type="radio"]').on('change', function() {
        // remove all active classes from targets
        $(this).closest('.radio-group').find('[type="radio"]').each(function(item) {
          $($(this).data('target')).removeClass('active');
        });
        
        //add active to current target
        $($(this).data('target')).addClass('active');
      });
    }
  };

// ---------------------------------------------------
// CHARTS

  var Charts = {
    init: function(data) {
      this.canvas = $('#chart');
      this.ctx = this.canvas.get(0).getContext("2d");
      this.chart = new Chart(this.ctx);
      this.chartData = data;
      this.legendCnt = $('.js-legend_container');


      // global settings
      Chart.defaults.global.responsive = true;
      Chart.defaults.global.scaleFontFamily = "'Open Sans', Helvetica, Arial, sans-serif";
      Chart.defaults.global.scaleFontColor = "#000";
      Chart.defaults.global.tooltipFontFamily = "'Open Sans', Helvetica, Arial, sans-serif";
      Chart.defaults.global.tooltipTitleFontFamily = "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif";

      if(this.canvas.data('chartype') === 'bar-rainbow') {
        this.barRainbowChart();
      } else if (this.canvas.data('chartype') === 'bar') {
        this.barChart();
      } else if (this.canvas.data('chartype') === 'doughnut') {
        this.doughnutChart();
      }

    },
    barRainbowChart: function(){

      this.chart.Bar(this.chartData, {
        barRainbow: true
      });

    },
    barChart: function(){

      this.legend = this.chart.Bar(this.chartData, {
        barRainbow: false,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li class=\"legend_item\"><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
      }).generateLegend();

      this.legendCnt.append(this.legend);
    },
    doughnutChart: function(){

      this.legend = this.chart.Doughnut(this.chartData, {
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li class=\"legend_item\"><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
      }).generateLegend();

      this.legendCnt.append(this.legend);
    }
  }
  