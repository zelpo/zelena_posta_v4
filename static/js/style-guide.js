$(document).ready(function(){

  $('.style-guide-body').parent().css('height', '100%');

  // Toggle.init();

  if(Modernizr.touch) {
    fixMobileHeight.init();
  }

});

// ----------------------------
// fixMobileHeight

  var fixMobileHeight = {
    init: function() {
      this.element = $('.style-guide-header')
      this.viewport = getViewport().height;

      this.binds();
      this.doResize();
    },
    binds: function(){
      var self = this;

      $(window).on('resize', function(){
        self.didResize = true;
      })

      setInterval(function(){
        if(self.didResize) {
          var newViewport = getViewport().height;

          if(Math.abs(self.viewport - newViewport) > 70) {
            self.doResize()
          }
          self.viewport = newViewport;
          self.didResize = false;
        }
      }, 250);

    },
    doResize: function(){
      this.element.css('height', (getViewport().height));
    }
  }

// ---------------------------------------------------
// getViewport

  var getViewport = function() {
    var viewport = {
      'width': 0,
      'height': 0
    }

    // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
    if (typeof window.innerWidth != 'undefined') {
      viewport.width = window.innerWidth;
      viewport.height = window.innerHeight

    }
   
    // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
    else if (typeof document.documentElement != 'undefined'
      && typeof document.documentElement.clientWidth !=
      'undefined' && document.documentElement.clientWidth != 0)
    {
      viewport.width = document.documentElement.clientWidth
      viewport.height = document.documentElement.clientHeight
    }
   
    // older versions of IE
    else {
      viewport.width = document.getElementsByTagName('body')[0].clientWidth
      viewport.height = document.getElementsByTagName('body')[0].clientHeight
    }

    return viewport;
  } // getViewport